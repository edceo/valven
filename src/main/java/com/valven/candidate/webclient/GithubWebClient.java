package com.valven.candidate.webclient;

import com.valven.candidate.constants.ExceptionCodes;
import com.valven.candidate.mapper.GithubMapper;
import com.valven.candidate.model.File;
import lombok.RequiredArgsConstructor;
import org.kohsuke.github.GHCommit;
import org.kohsuke.github.GHRepository;
import org.kohsuke.github.GitHub;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class GithubWebClient {

    private final GithubMapper githubMapper;

    public List<GHRepository> getRepositories(String userName, GitHub github) throws IOException {
        return github.getUser(userName).listRepositories().toList();
    }

    public List<GHCommit> getCommits(List<GHRepository> repositories, String userName) {

        List<GHCommit> commits = new ArrayList<>();
        repositories.forEach(project -> {
            try {
                List<GHCommit> collect = project
                        .listCommits()
                        .toList()
                        .stream()
                        .filter(ghCommit -> {
                            try {
                                return userName.equals(ghCommit.getCommitShortInfo().getCommitter().getName());
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                        })
                        .filter(ghCommit -> {
                            try {
                                return ghCommit.getCommitShortInfo().getCommitDate().after(Date.from(LocalDate.now().minusMonths(1000).atStartOfDay().toInstant(ZoneOffset.UTC)));
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                        })
                        .collect(Collectors.toList());
                commits.addAll(collect);
            } catch (IOException e) {
                throw new RuntimeException(ExceptionCodes.COMMIT_NOT_READ.getErrorMsg());
            }
        });
        return commits;
    }

    public List<File> getFiles(List<GHCommit> commits) {

        List<File> files = new ArrayList<>();

        commits.forEach(commit -> {
            try {
                List<File> collect = commit
                        .listFiles()
                        .toList()
                        .stream()
                        .map(file -> {
                            File fileDto = githubMapper.mapToFileFromFiles(file);
                            try {
                                fileDto.setCommit(githubMapper.mapToCommitFromGHCommit(commit));
                                return fileDto;
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        })
                        .collect(Collectors.toList());
                files.addAll(collect);
            } catch (IOException e) {
                throw new RuntimeException(ExceptionCodes.COMMIT_NOT_READ.getErrorMsg());
            }
        });
        return files;
    }
}
