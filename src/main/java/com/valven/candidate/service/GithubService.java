package com.valven.candidate.service;

import com.valven.candidate.configuration.TransactionHandler;
import com.valven.candidate.constants.ExceptionCodes;
import com.valven.candidate.constants.RepoProvider;
import com.valven.candidate.exceptions.GithubServiceException;
import com.valven.candidate.mapper.GithubMapper;
import com.valven.candidate.model.Commit;
import com.valven.candidate.model.Developer;
import com.valven.candidate.model.File;
import com.valven.candidate.model.Project;
import com.valven.candidate.repository.CommitRepository;
import com.valven.candidate.repository.DeveloperRepository;
import com.valven.candidate.repository.FileRepository;
import com.valven.candidate.repository.ProjectRepository;
import com.valven.candidate.webclient.GithubWebClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.kohsuke.github.GHCommit;
import org.kohsuke.github.GHRepository;
import org.kohsuke.github.GitHub;
import org.kohsuke.github.GitHubBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
@Log4j2
public class GithubService {

    @Value("${github.personal_access_token}")
    private String accessToken;
    private final GithubWebClient githubWebClient;
    private final GithubMapper githubMapper;
    private final DeveloperRepository developerRepository;
    private final ProjectRepository projectRepository;
    private final CommitRepository commitRepository;
    private final FileRepository fileRepository;
    private final TransactionHandler transactionHandler;

    public void saveAllRepositories(String userName) throws GithubServiceException {
        try {
            Optional<Developer> developerOptional = developerRepository.findById(userName);

            if (developerOptional.isEmpty()) {
                throw new GithubServiceException(ExceptionCodes.DEVELOPER_NOT_FOUND);
            }
            Developer developer = developerOptional.get();
            GitHub github = new GitHubBuilder().withOAuthToken(accessToken).build();
            List<GHRepository> repositories = githubWebClient.getRepositories(userName, github);
            List<Project> repositoriesDto = repositories.stream().map(ghRepository -> {
                Project project = githubMapper.mapToRepositoryFromGHRepository(ghRepository, RepoProvider.GITHUB);
                project.setDeveloper(developer);
                return project;
            }).collect(Collectors.toList());
            transactionHandler.runInTransaction(() -> projectRepository.saveAll(repositoriesDto));
            log.atInfo().log("Repositories saved successfully");

            List<GHCommit> commits = githubWebClient.getCommits(repositories, userName);
            List<Commit> commitsDto = commits.stream().map(ghCommit -> {
                try {
                    Commit commit = githubMapper.mapToCommitFromGHCommit(ghCommit);
                    commit.setDeveloper(developer);
                    return commit;
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }).collect(Collectors.toList());
            transactionHandler.runInTransaction(() -> commitRepository.saveAll(commitsDto));
            log.atInfo().log("Commits saved successfully");

            List<File> filesDto = githubWebClient.getFiles(commits);

            transactionHandler.runInTransaction(() -> fileRepository.saveAll(filesDto));
            log.atInfo().log("Files saved successfully");

        } catch (Exception e) {
            log.atError().log(e);
            throw new RuntimeException(e);
        }
    }
}
