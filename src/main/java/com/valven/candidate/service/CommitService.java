package com.valven.candidate.service;

import com.valven.candidate.dto.CommitDto;
import com.valven.candidate.mapper.CommitMapper;
import com.valven.candidate.model.Commit;
import com.valven.candidate.repository.CommitRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CommitService {

    private final CommitRepository commitRepository;
    private final CommitMapper commitMapper;

    public CommitDto getCommit(String hash) {
        Commit commit = commitRepository.findById(hash).orElseThrow();
        return commitMapper.mapToCommitDtoFromCommit(commit);
    }

    public List<CommitDto> getCommits() {
        Iterable<Commit> all = commitRepository.findAll();
        return commitMapper.mapToListCommitDtoFromIterableCommit(all);
    }
}
