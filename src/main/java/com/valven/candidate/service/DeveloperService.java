package com.valven.candidate.service;

import com.valven.candidate.dto.DeveloperDto;
import com.valven.candidate.mapper.DeveloperMapper;
import com.valven.candidate.model.Developer;
import com.valven.candidate.repository.DeveloperRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class DeveloperService {

    private final DeveloperRepository developerRepository;
    private final DeveloperMapper developerMapper;

    @Transactional
    public DeveloperDto createDeveloper(DeveloperDto developerDto) {
        Developer developer = developerMapper.mapToDeveloperFromDeveloperDto(developerDto);
        Developer savedDeveloper = developerRepository.save(developer);
        return developerMapper.mapToDeveloperDtoFromDeveloper(savedDeveloper);
    }

    @Transactional(readOnly = true)
    public DeveloperDto getDeveloper(String userName) {
        Developer developer = developerRepository.findById(userName).orElseThrow();
        return developerMapper.mapToDeveloperDtoFromDeveloper(developer);
    }
}
