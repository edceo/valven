package com.valven.candidate.constants;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum ExceptionCodes {
    REPOSITORY_NOT_READ(-1, "Repository Not Read"),
    COMMIT_NOT_READ(-2, "Commit Not Read"),
    FILE_NOT_READ(-3, "File Not Read"), DEVELOPER_NOT_FOUND(-4, "Developer Not Found");
    private final int errorCode;
    private final String errorMsg;
}
