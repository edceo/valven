package com.valven.candidate.constants;

public enum RepoProvider {
    GITHUB, GITLAB;
}
