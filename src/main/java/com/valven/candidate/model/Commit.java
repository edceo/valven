package com.valven.candidate.model;

import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * Commit class
 */
@Entity
@Table
@Data
public class Commit {
    @Column
    @Id
    private String hash;
    @Column
    private Date timeStamp;
    @Column
    private String message;
    @OneToMany(mappedBy = "commit")
    private List<File> fileList;
    @ManyToOne
    private Developer developer;
}
