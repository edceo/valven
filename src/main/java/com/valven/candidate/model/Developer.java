package com.valven.candidate.model;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Entity
@Table
@Data
public class Developer {
    @Column
    @Id
    private String userName;
    @Column
    private String email;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "developer")
    private List<Project> repositories;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "developer")
    private List<Commit> commits;
}
