package com.valven.candidate.model;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table
@Data
public class File {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String fileName;

    @Column(length = 150000)
    private String patch;

    @ManyToOne
    private Commit commit;
}
