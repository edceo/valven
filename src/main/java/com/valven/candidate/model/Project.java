package com.valven.candidate.model;

import com.valven.candidate.constants.RepoProvider;
import jakarta.persistence.*;
import lombok.Data;

/**
 * Project class
 */
@Entity
@Table
@Data
public class Project {
    @Id
    private Long id;

    @Column(name = "name")
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "repo_provider")
    private RepoProvider repoProvider;

    @ManyToOne
    private Developer developer;
}
