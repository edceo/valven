package com.valven.candidate.dto;

import java.util.List;

public record DeveloperDto(String userName, String email, List<CommitDto> commits) {
}
