package com.valven.candidate.dto;

import java.util.List;

public record CommitDto(String hash, String message, String timeStamp, String developerUserName, List<FileDto> files) {
}
