package com.valven.candidate.dto;

public record FileDto(String fileName, String patch) {
}
