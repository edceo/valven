package com.valven.candidate.exceptions;

import com.valven.candidate.constants.ExceptionCodes;

public class GithubServiceException extends Exception {
    private ExceptionCodes exceptionCodes;

    public GithubServiceException(ExceptionCodes exceptionCodes) {
        super(exceptionCodes.getErrorMsg());
        this.exceptionCodes = exceptionCodes;
    }
}
