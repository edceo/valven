package com.valven.candidate.mapper;

import com.valven.candidate.dto.DeveloperDto;
import com.valven.candidate.model.Developer;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DeveloperMapper {

    Developer mapToDeveloperFromDeveloperDto(DeveloperDto developerDto);
    DeveloperDto mapToDeveloperDtoFromDeveloper(Developer developer);
}
