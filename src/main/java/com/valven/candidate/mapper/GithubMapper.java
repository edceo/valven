package com.valven.candidate.mapper;

import com.valven.candidate.constants.RepoProvider;
import com.valven.candidate.model.Commit;
import com.valven.candidate.model.File;
import com.valven.candidate.model.Project;
import org.kohsuke.github.GHCommit;
import org.kohsuke.github.GHRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.io.IOException;

@Mapper(componentModel = "spring")
public interface GithubMapper {

    @Mapping(target = "repoProvider", source = "repoProvider")
    Project mapToRepositoryFromGHRepository(GHRepository ghRepository, RepoProvider repoProvider);

    @Mapping(target = "hash", expression = "java(commitInfo.getSHA1())")
    @Mapping(target = "timeStamp", source = "commitDate")
    @Mapping(target = "message", expression = "java(commitInfo.getCommitShortInfo().getMessage())")
    Commit mapToCommitFromGHCommit(GHCommit commitInfo) throws IOException;

    File mapToFileFromFiles(GHCommit.File file);

}
