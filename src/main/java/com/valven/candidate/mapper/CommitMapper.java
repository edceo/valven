package com.valven.candidate.mapper;

import com.valven.candidate.dto.CommitDto;
import com.valven.candidate.model.Commit;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CommitMapper {

    @Mapping(target = "developerUserName", source = "developer.userName")
    @Mapping(target = "files", source = "fileList")
    CommitDto mapToCommitDtoFromCommit(Commit commit);
    List<CommitDto> mapToListCommitDtoFromIterableCommit(Iterable<Commit> commits);
}
