package com.valven.candidate.controller;

import com.valven.candidate.exceptions.GithubServiceException;
import com.valven.candidate.service.GithubService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequiredArgsConstructor
@RequestMapping("/providers")
public class ProviderController {

    private final GithubService githubService;

    @PostMapping("/github/{userName}")
    public void initGithubUserRepositories(@PathVariable String userName) throws GithubServiceException {
        githubService.saveAllRepositories(userName);
    }
}
