package com.valven.candidate.controller;

import com.valven.candidate.dto.DeveloperDto;
import com.valven.candidate.service.DeveloperService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/developers")
public class DeveloperController {

    private final DeveloperService developerService;
    @PostMapping
    public DeveloperDto createDeveloper(@RequestBody DeveloperDto developerDto) {
        return developerService.createDeveloper(developerDto);
    }

    @GetMapping("/{userName}")
    public DeveloperDto getDeveloper(@PathVariable String userName) {
        return developerService.getDeveloper(userName);
    }
}
