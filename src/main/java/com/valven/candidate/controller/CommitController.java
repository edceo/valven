package com.valven.candidate.controller;

import com.valven.candidate.dto.CommitDto;
import com.valven.candidate.service.CommitService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController()
@RequiredArgsConstructor
@RequestMapping("/commits")
public class CommitController {

    private final CommitService commitService;

    @GetMapping
    public List<CommitDto> getCommits() {
        return commitService.getCommits();
    }

    @GetMapping("/{hash}")
    public CommitDto getCommit(@PathVariable String hash) {
        return commitService.getCommit(hash);
    }
}
