package com.valven.candidate.repository;

import com.valven.candidate.model.Project;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProjectRepository extends CrudRepository<Project, Long> {

    List<Project> findAllByDeveloper_UserName(String userName);

    Optional<Project> findByName(String name);
}
