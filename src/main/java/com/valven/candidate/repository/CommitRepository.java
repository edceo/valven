package com.valven.candidate.repository;

import com.valven.candidate.model.Commit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommitRepository extends CrudRepository<Commit, String> {
}
